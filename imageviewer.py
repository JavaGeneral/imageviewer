from tkinter import *
from PIL import ImageTk, Image

root = Tk()
root.title("Image Viewers")
root.iconbitmap("images/calc.ico")
my_img = ImageTk.PhotoImage(Image.open("images/myimage.png"))
my_img1 = ImageTk.PhotoImage(Image.open("images/image1.jpg"))
my_img2 = ImageTk.PhotoImage(Image.open("images/image2.jpg"))
my_img3 = ImageTk.PhotoImage(Image.open("images/image3.jpg"))
my_img4 = ImageTk.PhotoImage(Image.open("images/image4.jpg"))
images_list = [my_img, my_img1, my_img2, my_img3, my_img4]
my_label = Label(image=my_img)
my_label.grid(row=0, columnspan=3)


def main():
    button_back()
    button_forward()
    button_quit()
    root.mainloop()


def button_quit():
    button_quit_button = Button(root, text="Exit Program", command=root.quit)
    button_quit_button.grid(row=1, column=1)


def button_back():
    back_button = Button(root, text="<<", command=back, state=DISABLED)
    back_button.grid(row=1, column=0)


def button_forward():
    forward_button = Button(root, text=">>", command=lambda: forward(1))
    forward_button.grid(row=1, column=2)


def forward(image_number):
    global my_label
    global button_forward
    global button_back
    my_label.grid_forget()
    my_label = Label(image=images_list[image_number])
    button_forward = Button(root, text=">>", command=lambda: forward(image_number+1))
    button_back = Button(root, text="<<", command=lambda: back(image_number-1))

    if image_number == len(images_list)-1:
        button_forward = Button(root, text=">>", state=DISABLED)

    my_label.grid(row=0, columnspan=3)
    button_back.grid(row=1, column=0)
    button_forward.grid(row=1, column=2)


def back(image_number):
    global my_label
    global button_forward
    global button_back

    my_label.grid_forget()

    my_label = Label(image=images_list[image_number])
    button_forward = Button(root, text=">>", command=lambda: forward(image_number+1))
    button_back = Button(root, text="<<", command=lambda: back(image_number-1))

    if image_number == 0:
        button_back = Button(root, text="<<", state=DISABLED)

    my_label.grid(row=0, columnspan=3)
    button_back.grid(row=1, column=0)
    button_forward.grid(row=1, column=2)


if __name__ == '__main__':
    main()
